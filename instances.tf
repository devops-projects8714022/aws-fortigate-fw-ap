resource "aws_instance" "fortigate" {
  count         = 2
  ami           = var.ami_id
  instance_type = var.instance_type
  key_name      = var.key_name

  network_interface {
    network_interface_id = aws_network_interface.fortigate[count.index].id
    device_index         = 0
  }

  eip = aws_eip.fortigate[count.index].id

  tags = {
    Name = "FortiGate-${count.index}"
  }
}
