resource "aws_elb" "fortigate" {
  name               = var.elb_name
  availability_zones = ["${data.aws_availability_zones.available.names}"]

  listener {
    instance_port     = 443
    instance_protocol = "TCP"
    lb_port           = 443
    lb_protocol       = "TCP"
  }

  health_check {
    target              = "TCP:443"
    interval            = 30
    timeout             = 5
    healthy_threshold   = 2
    unhealthy_threshold = 2
  }

  instances = [aws_instance.fortigate[0].id, aws_instance.fortigate[1].id]

  tags = {
    Name = "fortigate-elb"
  }
}
