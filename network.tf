resource "aws_security_group" "fortigate_sg" {
  name        = "fortigate-sg"
  description = "Security group for FortiGate instances"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_eip" "fortigate" {
  count = 2

  vpc = true
}

resource "aws_network_interface" "fortigate" {
  count         = 2
  subnet_id     = element(var.subnet_ids, count.index)
  private_ips   = ["10.0.1.${count.index + 10}"]
  security_groups = [aws_security_group.fortigate_sg.id]
}
