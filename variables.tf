variable "aws_region" {
  description = "The AWS region to deploy into"
  type        = string
  default     = "us-west-2"
}

variable "vpc_id" {
  description = "The VPC ID where the FortiGate instances will be deployed"
  type        = string
}

variable "subnet_ids" {
  description = "List of subnet IDs to deploy into"
  type        = list(string)
}

variable "instance_type" {
  description = "The instance type for the FortiGate instances"
  type        = string
  default     = "c5.large"
}

variable "ami_id" {
  description = "The AMI ID for the FortiGate instances"
  type        = string
}

variable "key_name" {
  description = "The key pair name for SSH access"
  type        = string
}

variable "elb_name" {
  description = "The name of the Elastic Load Balancer"
  type        = string
}
