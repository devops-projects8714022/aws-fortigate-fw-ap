output "fortigate_instance_ids" {
  description = "The IDs of the FortiGate instances"
  value       = aws_instance.fortigate[*].id
}

output "fortigate_elb_dns" {
  description = "The DNS name of the ELB"
  value       = aws_elb.fortigate.dns_name
}
