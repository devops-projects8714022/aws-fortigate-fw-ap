provider "aws" {
  region = var.aws_region
}

module "fortigate_ap" {
  source        = "./fortigate-ap"
  aws_region    = var.aws_region
  vpc_id        = var.vpc_id
  subnet_ids    = var.subnet_ids
  instance_type = var.instance_type
  ami_id        = var.ami_id
  key_name      = var.key_name
  elb_name      = var.elb_name
}
